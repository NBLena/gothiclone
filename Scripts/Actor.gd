class_name Actor extends CharacterBody3D
# TODO: This now overlaps with the SpeakerItem class... 
# Best way to merge code is probably to make a child node
# that is a SpeakerItem and takes on the functions and duties
# of a SpeakerItem, while Actor does the CharacterBody3D stuff
#
# Does that sound good?

@export var movement_speed: float = 2.0
@export_file("*.json") var dialogJsonPath: String = ""

@export var target_waypoint: Node3D = null
@export var collision_object: CollisionObject3D = null

@onready var _actor_base: ActorBase  = $Actor
@onready var _navigation_agent: NavigationAgent3D = $Actor/NavigationAgent3D
@onready var _label = $Actor/InteractObjectLabel3D
@onready var _camera_focus_point = $Actor/CameraFocusPoint

var dialogTree: DialogTree = DialogTree.new()
var AiState: GameState.AiStates = GameState.AiStates.ROUTINE

# interactable_item variables -- remove this after merging!
var item_name = null
var focus_point

var dialog_file_changed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_label.text = self.name
	
	item_name = self.name
	focus_point = _camera_focus_point
	
	# prepare collision properties to be interacted with
	add_to_group("interactable")
	
	_navigation_agent.navigation_finished.connect(reached_waypoint)
	
	ready_after_loading()

func ready_after_loading():
	prepare_dialog_file()

	# Setup navigation agent
	# These values are tied to actors speed and navigation layout
	_navigation_agent.path_desired_distance = 0.5
	_navigation_agent.target_desired_distance = 0.5
	# Make sure to not await any function during _ready
	call_deferred("prepare_navigation_to_target_waypoint")

func prepare_dialog_file() -> void:
	if dialogJsonPath != "":
		var jsonFile = FileAccess.get_file_as_string(dialogJsonPath)
		var json = JSON.new()
		var jsonError = json.parse(jsonFile)
		if jsonError == OK:
			dialogTree.setTree(json.data)
			dialogTree.filePath = dialogJsonPath
			if dialogTree.isTreeOutdated():
				push_warning(dialogJsonPath, " The Dialog File is outdated and might break!")
		else:
			print("JSON Parse Error: ", json.get_error_message(), " in ", dialogJsonPath, " at line ", json.get_error_line())

func prepare_navigation_to_target_waypoint() -> void:
	await get_tree().physics_frame # Wait for the first physics frame so NavigationServer can sync
	var movement_target_position: Vector3 = target_waypoint.position if target_waypoint else Vector3(0,0,0)
	_navigation_agent.set_target_position(movement_target_position)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	if AiState != GameState.AiStates.ROUTINE:
		return

	# Move actor based on navigation agent data
	if target_waypoint == null or _navigation_agent.is_navigation_finished():
		set_velocity(Vector3.ZERO)
		move_and_slide()
		return

	var current_agent_position: Vector3 = global_transform.origin
	var next_path_position: Vector3 = _navigation_agent.get_next_path_position()

	look_at(next_path_position)
	rotate_y(PI) # Cause I set up the model the wrong way, rotate 180 degrees
	var new_velocity: Vector3 = next_path_position - current_agent_position
	new_velocity = new_velocity.normalized()
	new_velocity = new_velocity * movement_speed

	set_velocity(new_velocity)
	move_and_slide()

func interact() -> bool:
	if dialogJsonPath == "":
		printerr("No dialog set on this actor.")
		return false

	EventBus.StartDialog.emit(dialogTree, self)
	AiState = GameState.AiStates.DIALOG
	
	# false here means its okay for the player script to keep the actor as the selected item
	return false

func handle_script_signal(signal_string: String) -> void:
	printerr("The script gave the signal " + signal_string + ", but Actors do not handle signals yet.")

func playTalkAnimation() -> void:
	_actor_base.playTalkAnimation()

func dialogEnded() -> void:
	AiState = GameState.AiStates.ROUTINE
	if dialog_file_changed:
		prepare_dialog_file()

func highlight() -> void:
	_label.visible = true

func stop_highlight() -> void:
	_label.visible = false
	
func get_camera_focus_point() -> Vector3:
	return _camera_focus_point.global_position

func try_setting_waypoint(waypoint_name: String) -> void:
	for waypoint in get_tree().get_nodes_in_group("waypoints"):
		if waypoint.name == waypoint_name:
			target_waypoint = waypoint
			prepare_navigation_to_target_waypoint()
			return
	printerr("Could not find waypoint named " + waypoint_name)

func reached_waypoint() -> void:
	var tween = create_tween().bind_node(self)
	tween.tween_property(self, "global_rotation", target_waypoint.global_rotation, 1.0)
	target_waypoint = null

func switch_dialog_json_path(new_path: String) -> void:
	dialogJsonPath = "res://Dialog/" + new_path + ".json"
	dialog_file_changed = true
