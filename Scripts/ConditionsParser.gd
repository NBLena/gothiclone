class_name ConditionsParser

const debug = false
const reserved_tokens = ["ITEM", "QUEST", "DONEQUEST"]

var scriptName: String = ""
var callerName: String = ""
var conditionsLines: Array = []
var lineNumber: int
var negation: bool

func conditionsPass(conditionsCode: String, _scriptName: String, _callerName: String) -> bool:
	scriptName = _scriptName
	callerName = _callerName
	conditionsLines = conditionsCode.rsplit("\n", false) 

	lineNumber = 0
	for line in conditionsLines:
		negation = false
		lineNumber += 1
		line = line.lstrip(" ") # Removes spaces from beginning of line
		if line == "":
			continue
		parserDebug(">> Computing condition " + line)

		# Is this line a comment?
		if line[0] == "#":
			parserDebug("Line is a comment, ignored.")
			continue
		
		# Are we testing for negation?
		if line[0] == "!":
			parserDebug("Line starts with '!', negating this condition.")
			negation = true
			line = line.substr(1) # remove that character
		
		var tokens = line.split(" ")
		if len(tokens) > 1:
			match tokens[0]:
				"ITEM":
					# TODO: Join tokens 1-n, so that item name can be with spaces
					var inventoryItem = join_tokens(tokens, 1)
					var inventoryItemIsPresent = GameState.hasInventoryItemByName(inventoryItem)
					if conditionValueFails(inventoryItemIsPresent):
						parserDebug("Item " + inventoryItem + " was not found in player inventory.")
						return false
				"QUEST":
					var questIsActive = GameState.questLog.has(tokens[1])
					var questIsDone = GameState.doneQuestLog.has(tokens[1])
					
					# Only returns true if quest is ACTIVE. Completed quests should be checked with "QUESTDONE" (or something like it)
					if questIsDone or conditionValueFails(questIsActive) :
						parserDebug("Quest " + tokens[1] + " is not an active quest.")
						return false
				"DONEQUEST":
					var questIsDone = GameState.doneQuestLog.has(tokens[1])
					
					# Only returns true if quest is ACTIVE. Completed quests should be checked with "QUESTDONE" (or something like it)
					if conditionValueFails(questIsDone) :
						parserDebug("Quest " + tokens[1] + " is not a completed quest.")
						return false

				var unsupported_condition:
					parserError("Keyword " + unsupported_condition + " is not supported.")
			continue
		else:
			# Only one token!
			# Is it a reserved one with missing parameters?
			if reserved_tokens.has(line):
				parserError(line + " is reserved and cannot be a flag name. Did you forget parameters?")
				continue
			
			#Handle variable/flag
			var isStateVariableTruthy = GameState.stateStore.isStateTruthy(line)
			parserDebug("Checking if " + line + " is " + ("falsy" if negation else "truthy") + ", answer is " + str(isStateVariableTruthy))
			if conditionValueFails(isStateVariableTruthy):
				return false
	return true

func conditionValueFails(value) -> bool:
	# NOTE: Coming back, this looks confusing, but I assure you, it's correct!
	# Would probably read less confusing if negation was FALSE when line starts with "!"
	# Then you could write `(not value and negation) or (value and not negation)`, 
	# which reads more like it is meant.
	return (not value and not negation) or (value and negation)

func join_tokens(tokens: PackedStringArray, from: int = 0, to: int = 99999) -> String:
	var sub_tokens = tokens.slice(from, to + 1)
	return " ".join(sub_tokens)

func parserError(message) -> void:
	printerr("CONDITIONS ERROR IN ", scriptName , "/", callerName, " on LINE ", lineNumber, ": ", message)

func parserDebug(message) -> void:
	if debug:
		print(message)
