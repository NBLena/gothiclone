extends Node

enum AiStates {ROUTINE, DIALOG}

var playerActor: Player = null

var pauseMenuOpen = false
var inventoryOpen = false
var diaryOpen = false
var dialogMenuOpen = false
var cutscenePlaying = false

var Inventory: Array[Node3D] = []
var stateStore: StateStore = StateStore.new()

var questLog = {"Orientation": ["I just woke up. I need to get my bearings."]}
var doneQuestLog = {}

signal readyToLoadSaveData

func _ready() -> void:
	stateStore.init()
	EventBus.StartDialog.connect(startedDialog)
	EventBus.EndDialog.connect(endedDialog)

func pushMessageToScreenLog(msg: String) -> void:
	EventBus.pushMessageToScreenLog.emit(msg)
	
func anyMenuOpen() -> bool:
	return pauseMenuOpen or inventoryOpen or dialogMenuOpen or diaryOpen

func toggleInventory(open: bool) -> void:
	inventoryOpen = open

func toggleDiary(open: bool) -> void:
	diaryOpen = open

func togglePauseMenu(open: bool) -> void:
	pauseMenuOpen = open

func addInventoryItem(item: InventoryItem, showMessage: bool = true) -> void:
	item.enableInventoryLight()
	Inventory.append(item)
	if showMessage:
		pushMessageToScreenLog(item.inventory_item_name + " added to inventory")

func addInventoryItemByScene(item_scene_name: String, showMessage: bool = true) -> void:
	var item_scene = load("res://Scenes/Items/" + item_scene_name + ".tscn")
	var item = item_scene.instantiate()
	addInventoryItem(item, showMessage)

func removeInventoryItemByName(item_name: String) -> void:
	var item = getInventoryItemByName(item_name)
	if item == null:
		printerr("Tried to remove item that was not in the player inventory! Item Name: ", item_name)
		return
	pushMessageToScreenLog(item.inventory_item_name + " removed from inventory")
	Inventory.erase(item)
	
func getInventoryItemByName(item_name: String) -> Node3D:
	for item in Inventory:
		if item.item_name == item_name:
			return item
	return null

func hasInventoryItemByName(item_name: String) -> bool:
	return getInventoryItemByName(item_name) != null

func startedDialog(_dialogTree, _actor) -> void:
	dialogMenuOpen = true

func endedDialog() -> void:
	dialogMenuOpen = false

func isStateTruthy(stateName: String) -> bool:
	return stateStore.isStateTruthy(stateName)

func updateQuest(questId: String, diary_entry: String) -> void:
	# ignore updates to completed quests
	if doneQuestLog.has(questId):
		return
	
	if questLog.has(questId):
		if diary_entry == "DONE":
			doneQuestLog[questId] = questLog[questId]
			questLog.erase(questId)
			pushMessageToScreenLog("TASK COMPLETED")
			return
		else:
			# check if the entry already exists and ignore it if so
			if (questLog[questId] as Array[String]).has(diary_entry):
				return
			(questLog[questId] as Array[String]).append(diary_entry)
	else: # This is a quest_id we have not seen yet
		if diary_entry == "DONE":
			# NOTE: It is possible to design quests that end as soon as you make the first diary entry
			# This is not supported, since we assume the first entry is not the last. But maybe it will
			# be required later at some point?
			printerr("Can not mark quest as done, as quest_id " + questId + " was not known yet!")
			return
		questLog[questId] = [diary_entry]
	pushMessageToScreenLog("Diary updated")

func deferLoadingData(saveFileName: String) -> void:
	await get_tree().current_scene.tree_exited
	await get_tree().tree_changed
	self.call_deferred("emit_signal", "readyToLoadSaveData", saveFileName)
	togglePauseMenu(false)
