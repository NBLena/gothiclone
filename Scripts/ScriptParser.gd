class_name ScriptParser

const debug = false
const reserved_tokens = ["GIVE", "TAKE", "QUEST", "TEACH", "WAYPOINT", "DIALOG", "SIGNAL", "MESSAGE"]

var scriptName: String = ""
var callerName: String = ""
var scriptLines: Array = []

var lineNumber: int

# target is given no type, but should either be an Actor or a SpeakerItem
func runScript(scriptCode: String, target, _scriptName: String, _callerName: String) -> void:
	scriptName = scriptName
	callerName = callerName
	scriptLines = scriptCode.rsplit("\n", false)
	scriptDebug("Parsing a script! Here are the lines:")
	scriptDebug(scriptLines)
  
	lineNumber = 0
	for line in scriptLines:
		lineNumber += 1
		line = line.lstrip(" ") as String # Remove spaces from beginning of line
		scriptDebug(">> Computing line " + line)
		
		# Empty line?
		if len(line) == 0:
			continue

		# Is this line a comment?
		if line[0] == "#":
			scriptDebug("Line is a comment, ignored.")
			continue

		var tokens = line.split(" ")
		if len(tokens) > 1:
			match tokens[0]:
				"GIVE":
					# GIVE item_name [amount]
					if len(tokens) > 2:
						scriptError("Amount parameter for 'TAKE' is not supported yet.")
					GameState.addInventoryItemByScene(tokens[1])
				"TAKE":
					# TAKE item_name [amount]
					if len(tokens) > 2:
						scriptError("Amount parameter for 'TAKE' is not supported yet.")
					GameState.removeInventoryItemByName(tokens[1])
				"QUEST":
					# QUEST quest_id <diary entry>
					if len(tokens) <= 2:
						scriptError("There was only a questId, but no diary entry.")
						break
					GameState.updateQuest(tokens[1], join_tokens(tokens, 2))
				"TEACH":
					# TEACH skill_id
					scriptError("Command 'TEACH' is not supported yet.")
				"WAYPOINT":
					# WAYPOINT node3d_name
					if target is Actor:
						target.try_setting_waypoint(tokens[1])
					else:
						scriptError("Waypoints can only be set on Actors.")
				"DIALOG":
					# DIALOG path_to_json_file
					target.switch_dialog_json_path(tokens[1])
				"SIGNAL":
					# SIGNAL signal_string
					target.handle_script_signal(tokens[1])
				"MESSAGE":
					GameState.pushMessageToScreenLog(join_tokens(tokens, 1))
				var unsupported_command:
					scriptError("Command " + unsupported_command + " is not recognized!")
			continue

		# Only one token!
		# Is it a reserved one with missing parameters?
		if reserved_tokens.has(line):
			scriptError(line + " is reserved and cannot be a flag name. Did you forget parameters?")
			continue

		# Handle as a variable/flag to set to true or false 
		if line[len(line) - 1] == "!":
			line = line.substr(0, len(line) - 1)
			scriptDebug("Setting flag " + line + " to false!")
			EventBus.setStateBool.emit(line, false)
		else:
			scriptDebug("Setting flag " + line + " to true!")
			EventBus.setStateBool.emit(line, true)
  
	scriptDebug(" >>>> DONE WITH SCRIPT!")

func join_tokens(tokens: PackedStringArray, from: int = 0, to: int = 99999) -> String:
	var sub_tokens = tokens.slice(from, to + 1)
	return " ".join(sub_tokens)

func scriptError(message) -> void:
	printerr("SCRIPT ERROR IN ", scriptName , "/", callerName, " on LINE ", lineNumber, ": ", message)
	
func scriptDebug(message) -> void:
	if debug:
		print(message)
