extends Control

@onready var _dialogText = $DialogWindow/CenterContainer/Text
@onready var _dialogSpeaker = $DialogWindow/Speaker
@onready var _dialogOptionList = $OptionsWindow/ItemList
@onready var _animationPlayer = $AnimationPlayer

@export_node_path("Camera3D") var _dialogCamera

var scriptParser: ScriptParser = ScriptParser.new()
var conditionsParser: ConditionsParser = ConditionsParser.new()

var dialog: DialogTree = null 
var actor: Node3D = null
var actorCharacterBody: CharacterBody3D = null

var lastSelectedOption = null
var awaitInput = false # TODO: The use of this variable throughout this script is a bit confusing, consider a re-rewrite
var setupOptionsNext = false
var realOptionIndices = [] # TODO: This is a workaround for now
var nextSegment = null

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.StartDialog.connect(startDialog)

func _input(event):
	if not GameState.dialogMenuOpen or not awaitInput:
		return
	if event.is_action_released("ui_accept") or event.is_action_released("Interact"):
		nextDialog()

func startDialog(dialogTree: DialogTree, _opposite_actor: Node3D):
	dialog = dialogTree
	actor = _opposite_actor
	_dialogSpeaker.text = actor.item_name
	get_node(_dialogCamera).current = true
	awaitInput = false
	
	# NPCs should rotate actorCharacterBody towards player
	if actor is Actor:
		var originalTransform: Transform3D = actor.transform
		actor.look_at(GameState.playerActor.position)
		actor.rotate_y(PI) # TODO: Figure out why look_at turns actors away from player?
		var targetTransform = actor.transform
		actor.transform = originalTransform
		
		var tween = create_tween()
		tween.tween_property(actor, "transform", targetTransform, 1.0).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_SINE)
		tween.tween_callback(startFirstSegment)
	else:
		startFirstSegment()
	
func startFirstSegment():
	startSegment("start")

func startSegment(segmentName):
	# reset all segment tracking variables
	awaitInput = false
	setupOptionsNext = false
	nextSegment = null

	dialog.activeBranch = segmentName
	dialog.activeStringIndex = -1
	
	if dialog.hasBranchScript():
		scriptParser.runScript(dialog.getBranchScript(), actor, dialog.filePath, dialog.activeBranch)
	
	nextDialog()
	if dialog.getBranchStringsCount() > 0:
		_animationPlayer.play("FadeInDialog")

func nextDialog():
	dialog.advanceActiveBranchString()

	if dialog.hasReachedEndOfBranchStrings():
		endSegment()
		return

	if dialog.hasBranchStringCondition():
		if not conditionsParser.conditionsPass(dialog.getBranchStringCondition(), dialog.filePath, dialog.activeBranch):
			nextDialog()
			return

	_dialogText.text = "[center]" + dialog.getBranchStringText() + "[/center]"
	if actor is Actor:
		actor.playTalkAnimation()

func endSegment():
	# TODO: This is not good -- since some functions are set to be triggered by the end of the FadeInDialog animation, we
	# have to check here if the animation is played or not, and if not call the functions immediatly. Really needs some refactoring
	# NOTE: Branches with no strings, that link to another branch should also be supported! That should just silently link to that branch.
	if dialog.getBranchStringsCount() > 0:
		_animationPlayer.play_backwards("FadeInDialog")
		
		# do we have dialog options?
		if dialog.hasOptions():
			setupOptionsNext = true
			return
		
		# no options, maybe we jump to a different segment?
		if dialog.hasBranchNext():
			nextSegment = dialog.getBranchNext()
			return
	else:
		if dialog.hasOptions():
			setupOptions()
			return

		if dialog.hasBranchNext():
			startSegment(dialog.getBranchNext())
			return
	
	# none of the above, end dialog
	endDialog()

func endDialog():
	EventBus.EndDialog.emit()
	actor.dialogEnded()
	get_node(_dialogCamera).current = false
	actor = null
	actorCharacterBody = null

func setupOptions():
	_animationPlayer.play("FadeInOptions")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Input.warp_mouse(Vector2(640, 750))
	awaitInput = false
	_dialogOptionList.grab_focus()
	
	_dialogOptionList.clear()
	realOptionIndices.clear()
	var realIndex = 0
	for optionIndex in dialog.getOptionsCount():
		if dialog.hasOptionConditions(optionIndex):
			if not conditionsParser.conditionsPass(dialog.getOptionConditions(optionIndex), dialog.filePath, dialog.getOptionsString(optionIndex)):
				realIndex += 1
				continue
		
		realOptionIndices.append(realIndex)
		realIndex += 1
		var listIndex = _dialogOptionList.add_item(dialog.getOptionsString(optionIndex))
		_dialogOptionList.set_item_tooltip_enabled(listIndex, false) # no tooltips for dialog options please
	_dialogOptionList.select(0)

# Process Option after animations are done
func handleOption(index: int):
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	_dialogOptionList.release_focus()
	awaitInput = true
	
	var realIndex = realOptionIndices[index]
	
	if not dialog.hasOptionNext(realIndex):
		endDialog()
		return

	startSegment(dialog.getOptionNext(realIndex))
	
func _on_animation_player_animation_finished(anim_name):
	# TODO: This is confusing, because "FadeIn*" Animations are played backwards as well.
	# Should make seperate "FadeOut*" animations
	if anim_name == "FadeInDialog":
		awaitInput = true
		if setupOptionsNext:
			setupOptions()
			setupOptionsNext = false
		if nextSegment != null:
			startSegment(nextSegment)
			nextSegment = null
			
	elif anim_name == "FadeInOptions":
		if lastSelectedOption != null:
			handleOption(lastSelectedOption)
			lastSelectedOption = null

# Activate Option
func selectedOption(index):
	lastSelectedOption = index
	_animationPlayer.play_backwards("FadeInOptions")	

# Pressed Option
func _on_item_list_item_activated(index):
	selectedOption(index)

# Clicked Option
func _on_item_list_item_clicked(index, _at_position, _mouse_button_index):
	if _mouse_button_index == 1: # Left Mouse Button
		selectedOption(index)
