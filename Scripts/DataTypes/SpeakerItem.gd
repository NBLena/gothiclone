class_name SpeakerItem extends InteractableItem

@export_file("*.json") var dialogJsonPath: String = ""

var dialogTree: DialogTree = DialogTree.new()
var dialog_file_changed = false

func _ready() -> void:
	super()
	ready_after_loading()

func ready_after_loading():
	prepare_dialog_file()

func prepare_dialog_file() -> void:
	if dialogJsonPath != "":
		var jsonFile = FileAccess.get_file_as_string(dialogJsonPath)
		var json = JSON.new()
		var jsonError = json.parse(jsonFile)
		if jsonError == OK:
			dialogTree.setTree(json.data)
			dialogTree.filePath = dialogJsonPath
			if dialogTree.isTreeOutdated():
				push_warning(dialogJsonPath, " The Dialog File is outdated and might break!")
		else:
			print("JSON Parse Error: ", json.get_error_message(), " in ", dialogJsonPath, " at line ", json.get_error_line())

func interact() -> bool:
	if dialogJsonPath == "":
		printerr("No dialog set on this actor.")
		return false

	EventBus.StartDialog.emit(dialogTree, self)
	
	# false here means its okay for the player script to keep the actor as the selected item
	return false

func handle_script_signal(signal_string: String) -> void:
	push_warning("The script gave the signal " + signal_string + ", but no handler was defined on speakerItem")

func dialogEnded() -> void:
	if dialog_file_changed:
		prepare_dialog_file()

func get_camera_focus_point() -> Vector3:
	return global_position

func try_setting_waypoint(_waypoint_name: String) -> void:
	printerr("Tried to set a waypoint on a SpeakerItem, not an actor!")

func switch_dialog_json_path(new_path: String) -> void:
	dialogJsonPath = "res://Dialog/" + new_path + ".json"
	dialog_file_changed = true
