class_name InteractableItem extends Node3D

@export var item_name = ""
@export var collision_object: CollisionObject3D = null
@export var focus_point: Node3D = null
@onready var ITEMLABEL3D = preload("res://Scenes/UI/InteractObjectLabel3D.tscn")

var label3d: Label3D

func _ready():
	# prepare collision properties to be picked up as item
	add_to_group("interactable")
	
	# prepare label
	if label3d == null:
		label3d = ITEMLABEL3D.instantiate()
		add_child(label3d)
	label3d.text = item_name

	# set focus_point to a default
	if focus_point == null:
		focus_point = self

func highlight():
	label3d.visible = true

func stop_highlight():
	label3d.visible = false
