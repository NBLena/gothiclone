extends Control

@onready var _itemGrid = $Inventory/Panel/GridContainer
@onready var _itemDetailView = $Inventory/ItemDetailBG
@onready var _itemDetailViewName = %ItemName
@onready var _itemDetailViewDescription = %ItemDescription
@onready var _itemDetailViewSubViewPort = $Inventory/ItemDetailBG/HBoxContainer/CenterContainer/TextureRect/SubViewport
@onready var INVENTORYBUTTON = preload("res://Scenes/UI/InventoryItemButton.tscn")

const ROTATE_SPEED = 1.0

var selected_item_index = 0

func _ready():
	EventBus.InventoryItemButtonClicked.connect(showItemDetailView)

func showItemDetailView(item: InventoryItem):
	_itemDetailView.visible = true
	_itemDetailViewName.text = item.inventory_item_name
	_itemDetailViewDescription.text = item.item_description
	
	# Remove previous child (if there was one)
	var subViewPortChildren = _itemDetailViewSubViewPort.get_children()
	if len(subViewPortChildren) > 1:
		printerr("Inventory Detail View Subport had more than one child somehow!")
	for child in subViewPortChildren:
		_itemDetailViewSubViewPort.remove_child(child)
	
	# Add item as new child
	_itemDetailViewSubViewPort.add_child(item.duplicate())

# Rotate Item in detail view
func _process(delta):
	if GameState.inventoryOpen and _itemDetailView.visible:
		if len(_itemDetailViewSubViewPort.get_children()) > 0:
			var detailItem: InventoryItem = _itemDetailViewSubViewPort.get_child(0)
			if detailItem.item_mesh_path != null:
				detailItem.get_node(detailItem.item_mesh_path).rotate(Vector3.UP, ROTATE_SPEED * delta)


func _on_player_closed_menu():
	#	$MenuHint.visible = true
	$Inventory.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	# remove all items from inventory
	for child in _itemGrid.get_children():
		_itemGrid.remove_child(child)
		child.queue_free()


func _on_player_opened_menu():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# rebuild inventory nodes
	for item in GameState.Inventory:
		var newItemButton = INVENTORYBUTTON.instantiate()
		_itemGrid.add_child(newItemButton)
		newItemButton.set_item(item.duplicate())
	
#	$MenuHint.visible = false
	_itemDetailView.visible = false
	$Inventory.visible = true

	var firstItemButton = $Inventory.get_child(1) as TextureButton
	if firstItemButton:
		firstItemButton.grab_focus()
