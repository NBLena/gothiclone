extends TextureButton

@onready var _border = $BorderRect
@onready var _background = $BackgroundRect
@onready var _subViewport = $SubViewport

var buttonItem: InventoryItem = null

func set_item(item: InventoryItem):
	_subViewport.add_child(item)
	buttonItem = item

func _on_focus_entered():
	_border.visible = true


func _on_focus_exited():
	_border.visible = false


func _on_mouse_entered():
	_background.visible = true


func _on_mouse_exited():
	_background.visible = false

func _on_pressed():
	if buttonItem == null:
		printerr("Error: Inventory Button clicked but, item on it was null!")
		return
	EventBus.InventoryItemButtonClicked.emit(buttonItem)
