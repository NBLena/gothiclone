extends Node3D

@onready var _innerGimbal = $InnerGimbal
@onready var _springArm = $InnerGimbal/SpringArm3D

const cameraZoomMin = 3
const cameraZoomMax = 8
const mouse_sensitivity = 0.005

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):

func _input(event):
	if event is InputEventMouseMotion:
		if GameState.inventoryOpen and Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT):
			rotate_object_local(Vector3.UP, -event.relative.x * mouse_sensitivity)
		if not GameState.anyMenuOpen() or (GameState.inventoryOpen and Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT)):
			_innerGimbal.rotate_object_local(Vector3.RIGHT, event.relative.y * mouse_sensitivity)
		_innerGimbal.rotation.x = clamp(_innerGimbal.rotation.x, -0.7, 1.01)
	
	if GameState.anyMenuOpen():
		return
	
	if Input.is_action_pressed("ZoomCameraIn") and _springArm.spring_length > cameraZoomMin:
		_springArm.spring_length -= 0.5
	
	if Input.is_action_pressed("ZoomCameraOut") and _springArm.spring_length < cameraZoomMax:
		_springArm.spring_length += 0.5
		
