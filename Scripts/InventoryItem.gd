class_name InventoryItem extends InteractableItem

@export var inventory_item_name = ""
@export var item_description = ""
@export_node_path("Node3D") var item_mesh_path: NodePath # TODO: Can be replaced with %Mesh and making sure every item's mesh node is called Mesh

func _ready() -> void:
	super()
	label3d.text = inventory_item_name
	add_to_group("pickupable")

func interact() -> bool: # pick up item!
	var parent = get_parent()
	if parent:
		parent.remove_child(self)
	else:
		print("ERROR: Item " + item_name + " had it's interact function called but it doesn't have a parent. Was it already picked up?")
	GameState.addInventoryItem(self)
	return true

# Turn on light when item is in inventory, so it shows up correctly
func enableInventoryLight() -> void:
	var light = get_node_or_null("Camera3D/OmniLight3D")
	if light != null:
		light.visible = true
	else:
		print("ERROR: Item " + item_name + " does not have a camera or light set up and will not render correctly in inventory menu.")
