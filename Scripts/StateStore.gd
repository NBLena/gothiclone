class_name StateStore

var state: Dictionary = {}

const debug = false

func init() -> void:
	EventBus.setStateBool.connect(setStateBool)
	EventBus.setStateInt.connect(setStateInt)

func isStateTruthy(stateName: String) -> bool:
	return state.has(stateName) and state[stateName]

# Signal functions
func setStateBool(stateName: String, value: bool) -> void:
	state[stateName] = value
	debugPrint("Set a state variable! Here is the entire state so far:")
	debugPrint(state)

func setStateInt(stateName: String, value: int) -> void:
	state[stateName] = value

func debugPrint(msg) -> void:
	if debug:
		print(msg)
