class_name ActorBase extends Node3D

@onready var _animationTree = $AnimationTree

@export var actorBody: CharacterBody3D = null
@export var runThreshold = 4.0
@export var walkThreshold = 0.1

const idleAnimationName = "Idle"
const walkAnimationName = "Walking"
const runAnimationName = "Running"
const StrafeRightAnimationName = "Walking" #No strafing animation in Animation Tree yet

var animationState = idleAnimationName

func _process(_delta):
	if actorBody.AiState != GameState.AiStates.ROUTINE:
		return

	animationState = idleAnimationName

	if actorBody.get_real_velocity().z >= runThreshold or actorBody.get_real_velocity().z <= -runThreshold or actorBody.get_real_velocity().x >= runThreshold or actorBody.get_real_velocity().x <= -runThreshold:
		animationState = runAnimationName
	elif actorBody.get_real_velocity().z <= walkThreshold and actorBody.get_real_velocity().z >= -walkThreshold and actorBody.get_real_velocity().x <= walkThreshold and actorBody.get_real_velocity().x >= -walkThreshold:
		animationState = idleAnimationName
	else:
		animationState = walkAnimationName
	
	# set animation
	_animationTree["parameters/playback"].travel(animationState)

func playTalkAnimation() -> void:
	_animationTree["parameters/Talking/blend_position"] = randf_range(-1.0, 1.0)
	_animationTree["parameters/playback"].travel("Talking")
