extends Node

signal pushMessageToScreenLog

signal InventoryItemButtonClicked

signal StartDialog
signal EndDialog

signal setStateBool
signal setStateInt

signal checkStateTruthy
