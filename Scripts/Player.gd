class_name Player extends CharacterBody3D

@export var _gimbal: Node3D = null
@export var _innerGimbal: Node3D = null
@export var _dialogCamera: Camera3D = null
@export var _interactCollisionRay: RayCast3D

@export var mouse_player_rot_sensitivity = 0.04

@export var interpolateVerticalCamera = false

@export var moveSpeed = 100.0
@export var runSpeed = 400.0
@export var rotSpeed = 4.0
@export var mass = 130.0

@export var InterpolSpeed = 4.0
@export var VerInterpolSpeed = 1.0

@export var verInterpolThreshold = 1
var DEBUG_ITEM_RAYCAST = false
var verInterpolTimer = 0

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var vel = Vector3(0,0,0)
var deltaRotation = 0

var AiState = GameState.AiStates.ROUTINE
var lastItemsInRangeLength = 0
var itemsInRange: Array[Node3D] = []
var selectedItem: Node3D = null # TODO: Once Actor class is extended properly from same interactableItem class, set the type here

signal openedMenu # TODO: these should be called openedInventory and closedInventory. Gotta refactor at some point
signal closedMenu
signal openedDiary
signal closedDiary
signal openedPauseMenu
signal closedPauseMenu

# Called when the node enters the scene tree for the first time.
func _ready():
	# register as player actor
	GameState.playerActor = self
	
	EventBus.StartDialog.connect(prepareForDialogStart)
	EventBus.EndDialog.connect(prepareForDialogEnd)
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
# ====== HELPER FUNCTIONS
func player_can_move() -> bool:
	return not GameState.anyMenuOpen() and not GameState.cutscenePlaying

# ====== PROCESS FUNCTIONS
func _process(delta): # Called every frame. 'delta' is the elapsed time since the previous frame.
	
	# calculate gravity
	if not is_on_floor():
		vel.y -= gravity * mass * delta
	else:
		vel.y = 0
	
	# move stuff
	if player_can_move():
		process_camera(delta)
		handle_mouse_player_motion(delta)
		process_movement(delta)
	
		# do we need to choose a new selected item?
		if len(itemsInRange) != lastItemsInRangeLength:
			choose_selected_item()
		lastItemsInRangeLength = len(itemsInRange)

func process_camera(delta):
	# move camera behind player
	_gimbal.rotation = _gimbal.rotation.lerp(Vector3(0,0,0), delta * InterpolSpeed)
	if verInterpolTimer > verInterpolThreshold:
		_innerGimbal.rotation = _innerGimbal.rotation.lerp(Vector3(0,0,0), delta * VerInterpolSpeed)
	
	if interpolateVerticalCamera:
		verInterpolTimer += delta
	else: 
		verInterpolTimer = 0

func process_movement(delta):
	# rotation from keys
	rotate_object_local(Vector3.UP, deltaRotation * delta)
	_gimbal.rotate_object_local(Vector3.UP, -deltaRotation * delta)
	
	# movement
	set_velocity(vel.rotated(Vector3.UP, rotation.y) * delta)
	set_up_direction(Vector3.UP)
	
	move_and_slide()

func choose_selected_item(): # check which item in range is selected
	selectedItem = null
	
	var distance_of_selected = 0
	for item in itemsInRange:
		var distance = item.global_position.distance_to(global_position)
		_interactCollisionRay.target_position = _interactCollisionRay.to_local(item.focus_point.global_position)
		var path_to_item_is_not_blocked = not _interactCollisionRay.is_colliding()
		
		var collider = _interactCollisionRay.get_collider()
		
		# check if the object blocking is the item itself, in which case we allow it
		if not path_to_item_is_not_blocked and collider == item.collision_object:
			path_to_item_is_not_blocked = true
		# also, if the blocking object is a CSGCombiner3D, we allow it as well,
		# since those are subtracting and not generating geometry (at least not in the demo)
		if not path_to_item_is_not_blocked and typeof(collider) == typeof(CSGCombiner3D):
			path_to_item_is_not_blocked = true
		
		# This is buggy (not all colliders have names) and did not really help me figure out the issues with the raycast
		if DEBUG_ITEM_RAYCAST and not path_to_item_is_not_blocked:
			if not collider:
				print("collider was null or otherwise falsy")
			elif not item:
				print("item was null or otherwise falsy: ", item)
			else:
				prints(collider.name, item.collision_object.name, collider, item.collision_object, path_to_item_is_not_blocked)
		
		if distance > distance_of_selected and path_to_item_is_not_blocked:
			selectedItem = item
			distance_of_selected = distance
	
	for item in itemsInRange:
		if item == selectedItem:
			item.highlight()
		else:
			item.stop_highlight()

# ===== INPUT RELATED FUNCTIONS
func _input(event):
	# reset movement variables every frame
	vel.x = 0
	vel.z = 0
	interpolateVerticalCamera = false
	
	# process Input
	if player_can_move():
		check_movement_input()
	
	check_engine_input(event)
	
	if player_can_move() and Input.is_action_just_pressed("Interact") and selectedItem != null:
		var dontRepeatInteraction = selectedItem.interact()
		if dontRepeatInteraction:
			selectedItem = null

func handle_mouse_player_motion(delta):
	verInterpolTimer = 0
	
	var rotationAmount = Input.get_last_mouse_velocity().x * (rotSpeed * delta * 0.002)
	rotationAmount = clamp(rotationAmount, -rotSpeed * delta, rotSpeed * delta)
	rotate_object_local(Vector3.UP, -rotationAmount)
	_gimbal.rotate_object_local(Vector3.UP, rotationAmount)

func check_engine_input(event: InputEvent):
	if event.is_action_pressed("Pause Menu"):
		if (GameState.pauseMenuOpen):
			emit_signal("closedPauseMenu")
			if selectedItem:
				selectedItem.highlight()
			GameState.togglePauseMenu(false)
		elif not GameState.anyMenuOpen():
			emit_signal("openedPauseMenu")
			if selectedItem:
				selectedItem.stop_highlight()
			GameState.togglePauseMenu(true)
	
	if event.is_action_pressed("OpenInventory"):
		if (GameState.inventoryOpen):
			emit_signal("closedMenu")
			if selectedItem:
				selectedItem.highlight()
			GameState.toggleInventory(false)
		elif not GameState.anyMenuOpen():
			emit_signal("openedMenu")
			if selectedItem:
				selectedItem.stop_highlight()
			GameState.toggleInventory(true)
	
	if event.is_action_pressed("OpenDiary"):
		if GameState.diaryOpen:
			emit_signal("closedDiary")
			if selectedItem:
				selectedItem.highlight()
			GameState.toggleDiary(false)
		elif not GameState.anyMenuOpen():
			emit_signal("openedDiary")
			if selectedItem:
				selectedItem.stop_highlight()
			GameState.toggleDiary(true)
	
	if OS.is_debug_build() and event.is_action_pressed("ui_home"):
		print(GameState.stateStore.state)

func check_movement_input():
	if Input.is_action_pressed("Forward"):
		vel.z = moveSpeed
		interpolateVerticalCamera = true
		if Input.is_action_pressed("Sprint"):
			vel.z = runSpeed
	elif Input.is_action_pressed("Backward"):
		vel.z = -moveSpeed
		interpolateVerticalCamera = true

	if Input.is_action_pressed("StrafeLeft"):
		vel.x = moveSpeed
		vel.z = 0
		interpolateVerticalCamera = true
	elif Input.is_action_pressed("StrafeRight"):
		vel.x = -moveSpeed
		vel.z = 0
		interpolateVerticalCamera = true

	if Input.is_action_pressed("TurnRight"):
		deltaRotation = -rotSpeed
	elif Input.is_action_pressed("TurnLeft"):
		deltaRotation = rotSpeed
	else:
		deltaRotation = 0

# ===== SIGNAL HANDLER FUNCTIONS
func _on_interact_area_body_entered(body):
	if body.is_in_group("interactable"):
		itemsInRange.append(body)
	else:
		var item = body.get_parent()
		if item.is_in_group("interactable"):
			itemsInRange.append(item)

func _on_interact_area_body_exited(body):
	if body.is_in_group("interactable"):
		itemsInRange.erase(body)
		body.stop_highlight()
	else:
		var item = body.get_parent()
		if item.is_in_group("interactable"):
			itemsInRange.erase(item)
			item.stop_highlight()

func prepareForDialogStart(_tree, actorRoot):
	if selectedItem:
		selectedItem.stop_highlight()
	
	# Align dialog camera to look at actor
	_dialogCamera.look_at(actorRoot.get_camera_focus_point())

func prepareForDialogEnd():
	if selectedItem != null:
		selectedItem.highlight()
