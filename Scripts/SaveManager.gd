class_name SaveManager extends Node

# regex to get the filename from a path
var regex: RegEx = RegEx.create_from_string("[ \\w-]+?(?=\\.)")

const SAVE_VERSION = 1

func _ready():
	GameState.readyToLoadSaveData.connect(load_data)

func save_game(file: FileAccess) -> void:
	# Save File Version
	file.store_16(SAVE_VERSION)
	
	# Player Position
	file.store_var(GameState.playerActor.position)
	file.store_var(GameState.playerActor.rotation)
	file.store_var(GameState.playerActor.scale)
	
	# State variables
	file.store_var(GameState.stateStore.state)
	
	# Inventory
	var item_scene_names: Dictionary = {}
	for item in GameState.Inventory:
		if item.scene_file_path == "":
			printerr(item.item_name, " could not be saved; scene file path is not set.")
			continue
		var regex_result = regex.search(item.scene_file_path)
		var item_scene_name = regex_result.get_string()
		item_scene_names[item.name] = item_scene_name
	file.store_var(item_scene_names)
	
	# Questlog
	file.store_var(GameState.questLog)
	file.store_var(GameState.doneQuestLog)
	
	# Speaker Items and Actors
	var speaker_item_dialog_json_paths: Dictionary = {}
	var actor_data: Dictionary = {}
	for interactableItem in get_tree().get_nodes_in_group("interactable"):
		if interactableItem is SpeakerItem:
			speaker_item_dialog_json_paths[interactableItem.name] = interactableItem.dialogJsonPath
		if interactableItem is Actor:
			actor_data[interactableItem.name] = {
				"dialog_path": interactableItem.dialogJsonPath,
				"transform": interactableItem.global_transform,
				"target_waypoint": interactableItem.target_waypoint,
				"ai_state": interactableItem.AiState
			}
	file.store_var(speaker_item_dialog_json_paths)
	file.store_var(actor_data)
	
	# save later (can this be done by saving the level?):
		# actors
			# waypoint / navigation
		# doors
			# add them to a group and play a _loading version of their animation that just makes them open, like with computer_desk
	file.close()

func load_game(saveFileName: String) -> void:
	# Read File Version
	var file = FileAccess.open(saveFileName, FileAccess.READ)
	var save_file_version = file.get_16()
	if save_file_version < SAVE_VERSION:
		print("WARNING: Save file is of older version! Expect bugs!")
	file.close()
	
	# Defer loading of actual data until AFTER the scene was reset
	# Doing this via GameState because that should not be unloaded
	# with scene reset, unlike this script right here, since it
	# it is a child of the PauseMenu of the Player... of the scene
	GameState.deferLoadingData(saveFileName)
	
	# Reset Level
	get_tree().reload_current_scene()
	
# Gets called from GameState.deferLoadingData() via signal readyToLoadSaveData
func load_data(saveFileName: String) -> void:
	var file = FileAccess.open(saveFileName, FileAccess.READ)
	var _save_file_version = file.get_16() # we wont do anything with the version number here, but we need to read it
	
	prints("Loading Save File", saveFileName)
	# Player Position
	GameState.playerActor.position = file.get_var()
	GameState.playerActor.rotation = file.get_var()
	GameState.playerActor.scale = file.get_var()
	
	# State Variables
	GameState.stateStore.state = file.get_var()
	
	# Inventory
	var inventory_item_scenes = file.get_var() as Dictionary
	var all_pickupable_items = get_tree().get_nodes_in_group("pickupable")
	GameState.Inventory.clear()
	for item_name in inventory_item_scenes:
		GameState.addInventoryItemByScene(inventory_item_scenes[item_name], false)
		# check if item in level matches name with item in inventory
		for pickupable_item in all_pickupable_items:
			if pickupable_item.name == item_name:
				pickupable_item.queue_free() # if so, delete it (player picked it up previously)
	
	# Questlog
	GameState.questLog = file.get_var()
	GameState.doneQuestLog = file.get_var()
	
	# Speaker Items and Actors
	var speaker_item_dialog_json_paths = file.get_var() as Dictionary
	var actor_data = file.get_var() as Dictionary
	for interactableItem in get_tree().get_nodes_in_group("interactable"):
		if interactableItem is SpeakerItem:
			for speakerItem in speaker_item_dialog_json_paths:
				if interactableItem.name == speakerItem:
					interactableItem.dialogJsonPath = speaker_item_dialog_json_paths[speakerItem]
					interactableItem.prepare_dialog_file()
		if interactableItem is Actor:
			for actor in actor_data:
				if interactableItem.name == actor:
					interactableItem.dialogJsonPath = actor_data[actor]["dialog_path"]
					interactableItem.global_transform = actor_data[actor]["transform"]
					interactableItem.target_waypoint = actor_data[actor]["target_waypoint"]
					interactableItem.AiState = actor_data[actor]["ai_state"]
					interactableItem.ready_after_loading()
	
	file.close()

	# == animations cleanup ==
	
	# Restore state of the computer desk after the cutscene
	if GameState.isStateTruthy("cutscene_played"):
		var cutscene_node = get_tree().current_scene.find_child("Cutscene") as AnimationPlayer
		print("playing the cutscene thing!")
		cutscene_node.play("Cutscene_Quarters_Loaded")
