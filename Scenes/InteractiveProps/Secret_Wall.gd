extends InteractableItem

func _ready() -> void:
	label3d = $InteractObjectLabel3D
	super()

func interact() -> bool:
	print("lets goooo")
	# if you have omnitool
	prints("have omnitool? ", GameState.hasInventoryItemByName("OmniTool"))
	prints("learned omnitool? ", GameState.stateStore.isStateTruthy("learned_omnitool"))
	if GameState.hasInventoryItemByName("OmniTool") and GameState.stateStore.isStateTruthy("learned_omnitool"):
		print("Everythings a go! Delete Wall!")
		# if you learned omnitool
		$SecretTunnel2.queue_free()
		$Secret_Wall_Collider/CollisionShape3D.queue_free()
		$Secret_Wall_Collider.queue_free()
	return true
