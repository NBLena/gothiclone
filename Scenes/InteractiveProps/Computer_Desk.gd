extends SpeakerItem

@export var thisLabel3D: Label3D = null
@export var connectionStatusImage: TextureRect = null
@export var onlineStatusImage: Texture2D = null

func _ready() -> void:
	# Handle label ourselves
	label3d = thisLabel3D
	label3d.text = item_name
	
	super()

func get_camera_focus_point() -> Vector3:
	return $Screen_On.global_position

func interact() -> bool:
	return super()

func handle_script_signal(signal_string: String) -> void:
	if signal_string == "show_image":
		connectionStatusImage.visible = true
	if signal_string == "contact_online":
		connectionStatusImage.texture = onlineStatusImage

func dialogEnded() -> void:
	connectionStatusImage.visible = false
	super()
