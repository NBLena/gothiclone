extends InteractableItem

var isOpen = false
@export var stateCondition: String = ""

func _ready() -> void:
	super()

func interact() -> void:
	if $AnimationPlayer.is_playing():
		return
	if stateCondition != "" and not GameState.isStateTruthy(stateCondition):
		GameState.pushMessageToScreenLog("The door does not respond.")
		prints("This door does not open, because following state is not truthy:", stateCondition)
		return


	if isOpen:
		$AnimationPlayer.play("close")
	else:
		$AnimationPlayer.play("open")
	isOpen = !isOpen
