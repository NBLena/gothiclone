extends CenterContainer

@export var _filesList: ItemList = null
@export var _newFileLineEdit: LineEdit = null
@export var _saveManager: SaveManager = null
const saveFilesPath: String = "user://saves"


func updateSaveFileUI():
	# erase items in _filesList
	_filesList.clear()
	# prepare _newFileLineEdit
	_newFileLineEdit.text = ""
	
	# try to open save folder
	var dir = DirAccess.open(saveFilesPath)
	# check if save folder exists
	if dir:
		# get the list of files in the save folder
		dir.list_dir_begin()
		var files = dir.get_files()
		for file_name in files:
				# turn file names into items and add to _filesList
				_filesList.add_item(file_name)
	else:	# if not, create it
		print("No save folder exists yet. Creating one.")
		DirAccess.make_dir_absolute(saveFilesPath)

func _on_third_person_player_opened_pause_menu():
	updateSaveFileUI()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	visible = true

func _on_third_person_player_closed_pause_menu():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	visible = false

# Quit Game
func _on_quit_button_pressed():
	get_tree().quit()

func _on_save_button_pressed():
	var newFileName = null
	if _newFileLineEdit.text != "":
		newFileName = _newFileLineEdit.text
	else:
		var selectedIndices = _filesList.get_selected_items()
		if len(selectedIndices) == 0:
			return
		newFileName = _filesList.get_item_text(selectedIndices[0])
	if not newFileName:
		return
		
	print("Saving game under " + newFileName)
	var newSaveFile = FileAccess.open(saveFilesPath + "/" + newFileName, FileAccess.WRITE)
	_saveManager.save_game(newSaveFile)
	newSaveFile.close()
	updateSaveFileUI()

func _on_load_button_pressed():
	var selectedIndices = _filesList.get_selected_items()
	if len(selectedIndices) == 0:
		return
	var savefileName = _filesList.get_item_text(selectedIndices[0])
	var fullSaveFilePath = saveFilesPath + "/" + savefileName

	_saveManager.load_game(fullSaveFilePath)

	_filesList.deselect_all()
	Input.action_press("Pause Menu") # Close the M enu

# Pressed enter on new save file
func _on_new_file_line_edit_text_submitted(_new_text):
	_on_save_button_pressed()

func _on_delete_button_pressed():
	for index in _filesList.get_selected_items():
		DirAccess.remove_absolute(saveFilesPath + "/" + _filesList.get_item_text(index))
	updateSaveFileUI()
	
func _on_files_list_item_selected(_index):
	_newFileLineEdit.text = ""
