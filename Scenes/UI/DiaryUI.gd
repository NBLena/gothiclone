extends Control

@export var questList: ItemList = null
@export var diaryEntriesList: BoxContainer = null
@export var protoDiaryEntry: Label = null
@export var entrySeperator: HSeparator = null

func _on_third_person_player_closed_diary() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	visible = false

func _on_third_person_player_opened_diary() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	update_diary()
	visible = true

func update_diary() -> void:
	questList.clear()
	
	# quests
	for entry in GameState.questLog:
		questList.add_item(entry)
	
	# indicator for completed quests next
	var i = questList.add_item("Completed Quests", null, false)
	questList.set_item_disabled(i, true)
	
	# done quests
	for entry in GameState.doneQuestLog:
		questList.add_item(entry)

func update_diary_entries(entries: Array) -> void:
	# remove all current entries
	for child in diaryEntriesList.get_children():
		diaryEntriesList.remove_child(child)
		child.queue_free()
		
	for entry in entries:
		var newLabel = protoDiaryEntry.duplicate()
		newLabel.text = entry
		newLabel.visible = true
		diaryEntriesList.add_child(newLabel)
		var newSeperator = entrySeperator.duplicate()
		newSeperator.visible = true
		diaryEntriesList.add_child(newSeperator)

func _on_quest_list_item_clicked(index, _at_position, mouse_button_index) -> void:
	# only handle left mouse button clicks
	if mouse_button_index > 1:
		return
	
	var questLogCount = len(GameState.questLog)
	
	if index < questLogCount:
		var questKey = questList.get_item_text(index)
		var entries = GameState.questLog[questKey]
		update_diary_entries(entries)

	if index > questLogCount:
		var questKey = questList.get_item_text(index)
		var entries = GameState.doneQuestLog[questKey]
		update_diary_entries(entries)
