extends Label

func startTimer() -> void:
	var timer = get_node("Timer")
	timer.timeout.connect(removeSelf)
	timer.start()
	
func removeSelf() -> void:
	queue_free()
