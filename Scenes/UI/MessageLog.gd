extends Control

@export var protoMessage: Label = null
@export var messageContainer: VBoxContainer = null

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.pushMessageToScreenLog.connect(addMessage)

func addMessage(msg: String) -> void:
	var newMessage = protoMessage.duplicate()
	newMessage.text = msg
	newMessage.visible = true
	messageContainer.add_child(newMessage)
	newMessage.startTimer()
