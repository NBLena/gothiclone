extends Area3D

@export var animation_player: AnimationPlayer = null

func _on_body_entered(body: Node3D):
	if body is Player and not GameState.isStateTruthy("cutscene_played"):
		GameState.cutscenePlaying = true
		GameState.stateStore.setStateBool("cutscene_played", true)
		GameState.pushMessageToScreenLog("[PINOCCIO PROGRAM TAKING OVER...]")
		animation_player.play("Cutscene_Quarters")

func _input(event):
	if GameState.cutscenePlaying and event.is_action_pressed("Cancel"):
		animation_player.advance(20.0)

func cutsceneFinished() -> void:
	GameState.cutscenePlaying = false
