@tool
extends Button

@onready var scriptIconActive = load("res://addons/gothicLike_dialog_editor/Assets/Script.svg")
@onready var scriptIconInactive = load("res://addons/gothicLike_dialog_editor/Assets/ScriptCreate.svg")
@onready var scriptIconSelected = load("res://addons/gothicLike_dialog_editor/Assets/ScriptExtend.svg")

func setScriptButtonActive(active: bool) -> void:
	icon = scriptIconActive if active else scriptIconInactive

func setScriptButtonSelected() -> void:
	icon = scriptIconSelected