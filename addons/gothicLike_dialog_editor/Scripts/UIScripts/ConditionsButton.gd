@tool
extends Button

@onready var conditionIconActive = load("res://addons/gothicLike_dialog_editor/Assets/FlowContainer.svg")
@onready var conditionIconInactive = load("res://addons/gothicLike_dialog_editor/Assets/FlowContainer_Gray.svg")
@onready var conditionIconSelected = load("res://addons/gothicLike_dialog_editor/Assets/FlowContainer_Pink.svg")

signal openOptionsConditions

# This is meant for the options conditions buttons
func _on_pressed():
	openOptionsConditions.emit(get_node("..").my_index())

func setConditionsButtonActive(active: bool) -> void:
	icon = conditionIconActive if active else conditionIconInactive

func setConditionsButtonSelected() -> void:
	icon = conditionIconSelected
