@tool
extends HBoxContainer

@onready var _conditionsButton = get_node("OptionConditionsButton")

func my_index():
	return get_index() - get_node("../..").OPTIONS_OFFSET_IN_DIALOG_CONTENT_CONTAINER

func setOptionConditionsButtonActive(active: bool) -> void:
	_conditionsButton.setConditionsButtonActive(active)

func setOptionConditionsButtonSelected() -> void:
	_conditionsButton.setConditionsButtonSelected()