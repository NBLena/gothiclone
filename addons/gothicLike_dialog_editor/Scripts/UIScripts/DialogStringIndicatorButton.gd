@tool
extends Button


signal pressedStringIndicatorButton

func _on_pressed():
	emit_signal("pressedStringIndicatorButton", get_index())
