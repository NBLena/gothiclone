@tool
extends Button

signal goToBranch

func _on_pressed():
	var picker = get_node_or_null("../OptionBranchPicker")
	if picker == null:
		picker = get_node_or_null("../BranchPicker")
	if picker == null:
		printerr("Could not find the BranchPicker button in the same branch of the scene tree!")
		return
	var branchTitle = picker.text
	goToBranch.emit(branchTitle)
