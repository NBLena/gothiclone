@tool
extends Button

signal openBranchPicker

func _on_pressed():
	openBranchPicker.emit(self)

func getGoToNextButton():
	var button = get_node_or_null("../OptionGoToNextButton")
	if button == null:
		button = get_node_or_null("../BranchOptionGoToNextButton")
	if button == null:
		printerr("Could not find GoToNextButton for this SegmentPicker Button!")
	return button
		
func activateGoButton():
	getGoToNextButton().disabled = false

func deactivateGoButton():
	getGoToNextButton().disabled = true
