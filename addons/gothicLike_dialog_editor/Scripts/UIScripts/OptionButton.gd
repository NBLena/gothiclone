@tool
extends Button

signal deleteOption
signal moveOption

func _on_pressed():
	if self.name == "OptionMoveUp":
		moveOption.emit(getOptionIndex(), getOptionIndex() - 1)
	elif self.name == "OptionMoveDown":
		moveOption.emit(getOptionIndex(), getOptionIndex() + 1)
	elif self.name == "OptionDeleteButton":
		deleteOption.emit(getOptionIndex())
	else:
		printerr("OptionButton script was attached to a button it doesnt support.")

func getOptionIndex():
	return get_node("..").my_index()
