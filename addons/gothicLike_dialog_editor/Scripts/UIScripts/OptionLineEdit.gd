@tool
extends LineEdit

signal optionTextChanged

func _on_text_changed(new_text):
	optionTextChanged.emit(getOptionIndex(), new_text)

func getOptionIndex():
	return get_node("..").my_index()
