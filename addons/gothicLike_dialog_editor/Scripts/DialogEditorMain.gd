@tool
extends Control

const OPTIONS_OFFSET_IN_DIALOG_CONTENT_CONTAINER = 4

var filePath = null
var dialogTree: DialogTree = DialogTree.new()
var activeBranchPickerButton = null
var activeOption = -1

@onready var _branchPickerItemList = %BranchPickerItemList
@onready var _branchPicker = %BranchPicker
@onready var _branchPickerWindow = %BranchPickerWindow

func _ready():
	_branchPicker.openBranchPicker.connect(openBranchPicker)
	%BranchOptionGoToNextButton.goToBranch.connect(goToBranch)
	%OptionContainer.visible = false

func create_new_dialog_file(newDialogFilePath):
	if FileAccess.file_exists(newDialogFilePath):
		printerr("File exists already! Aborting creating new dialog file")
		return
	
	var newFile = FileAccess.open(newDialogFilePath, FileAccess.WRITE)
	var newDialogTree = DialogTree.CLEAN_TREE.duplicate()
	var newDialogTreeJsonData = JSON.stringify(newDialogTree)
	newFile.store_string(newDialogTreeJsonData)
	newFile.close()
	filePath = newDialogFilePath
	# load new file
	_on_file_dialog_file_selected(newDialogFilePath)

func save_dialog_file():
	if filePath != null:
		writeDialogToFile(filePath)
		return
	print("No dialog file loaded in dialog editor. Nothing saved.")

func writeDialogToFile(saveFilePath): # name idea: write dialog to file?
	var newFile = FileAccess.open(saveFilePath, FileAccess.WRITE)
	newFile.store_string(JSON.stringify(dialogTree.tree))
	newFile.close()

func load_dialog_file(dialogJsonPath):
	filePath = dialogJsonPath
	# Prepare DialogTree
	var jsonFile = FileAccess.get_file_as_string(dialogJsonPath)
	var json = JSON.new()
	var jsonError = json.parse(jsonFile)
	if jsonError == OK:
		dialogTree.setTree(json.data)
		if dialogTree.isTreeOutdated():
			dialogTree.migrateDialogTree()
		dialogTree.activeBranch = "start"
		update_tree_view()
		process_branch("start")
		enableOperationButtons()
	else:
		printerr("JSON Parse Error: ", json.get_error_message(), " in ", dialogJsonPath, " at line ", json.get_error_line())

func update_tree_view():
	%Tree.clear()
	var segmentHeader: TreeItem = %Tree.create_item()
	segmentHeader.set_text(0, filePath)
	
	_branchPickerItemList.clear()
	_branchPickerItemList.add_item("[Empty]")
	
	for branch in dialogTree.getBranches():
		var newBranch: TreeItem = %Tree.create_item()
		newBranch.set_text(0, branch)
		_branchPickerItemList.add_item(branch)
		if (branch == dialogTree.activeBranch):
			newBranch.select(0)

func process_branch(branch: String = dialogTree.activeBranch):
	%DialogTextEdit.text = ""
	%NextStringButton.disabled = true
	%PrevStringButton.disabled = true
	_branchPicker.visible = true
	%BranchVSeparator.visible = true
	%BranchOptionGoToNextButton.visible = true
	%OptionSettingsContainer.visible = false
	activeOption = -1
	
	dialogTree.activeBranch = branch
	dialogTree.activeStringIndex = 0
	
	# Reset dialog options
	while %DialogContentContainer.get_child_count() > OPTIONS_OFFSET_IN_DIALOG_CONTENT_CONTAINER:
		%DialogContentContainer.remove_child(%DialogContentContainer.get_child(-1))
	
	# Reset dialog strings gadget
	for child in %DialogStringBoxContainer.get_children():
		if child.is_in_group("DialogEditorStringIndicatorButtons"):
			%DialogStringBoxContainer.remove_child(child)
	
	if dialogTree.hasBranchStrings():
		var strings = dialogTree.getBranchStrings()
		updateDialogTextUI(dialogTree.getBranchStringText(branch, 0))
		# Duplicate %DialogStringIndicatorButton for as many strings as there are
		var index = 0
		for string in strings:
			addDialogIndicatorButton(index)
			index += 1
	else:
		addDialogIndicatorButton(0)
	
	# Update script and condition buttons
	var hasBranchScript = dialogTree.hasBranchScript()
	%BranchScriptButton.setScriptButtonActive(hasBranchScript)
	updateStringConditionButton()
	
	setDialogIndicatorButtonActive()
	
	print("options: ", dialogTree.getOptions())
	if dialogTree.hasOptions():
		print("checking options")
		var optionIndex = 1
		for option in dialogTree.getOptions():
			var newOptionContainer = %OptionContainer.duplicate()
			newOptionContainer.get_node("OptionLineEdit").text = option.string
			newOptionContainer.visible = true
			newOptionContainer.get_node("OptionLineEdit").optionTextChanged.connect(apply_option_line_edit_changes)
			newOptionContainer.get_node("OptionBranchPicker").openBranchPicker.connect(openBranchPicker)
			newOptionContainer.get_node("OptionGoToNextButton").goToBranch.connect(goToBranch)
			newOptionContainer.get_node("OptionDeleteButton").deleteOption.connect(deleteOption)
			newOptionContainer.get_node("OptionMoveDown").moveOption.connect(moveOption)
			newOptionContainer.get_node("OptionMoveUp").moveOption.connect(moveOption)
			newOptionContainer.get_node("OptionConditionsButton").openOptionsConditions.connect(openOptionsConditions)
			if option.get("next"):
				newOptionContainer.get_node("OptionBranchPicker").text = option.next
				newOptionContainer.get_node("OptionGoToNextButton").disabled = false
			if optionIndex == 1:
				newOptionContainer.get_node("OptionMoveUp").disabled = true
			if optionIndex == dialogTree.getOptionsCount():
				newOptionContainer.get_node("OptionMoveDown").disabled = true
				
			%DialogContentContainer.add_child(newOptionContainer)
			# change elements that need the container to be inside the tree already
			var optionHasConditions = dialogTree.hasOptionConditions(optionIndex - 1, branch)
			newOptionContainer.setOptionConditionsButtonActive(optionHasConditions)
			
			optionIndex += 1
		_branchPicker.visible = false
		%BranchOptionGoToNextButton.visible = false
		%BranchVSeparator.visible = false
	else:
		if dialogTree.hasBranchNext():
			_branchPicker.text = dialogTree.getBranchNext()
			%BranchOptionGoToNextButton.disabled = false
		else:
			_branchPicker.text = "[Empty]"
			%BranchOptionGoToNextButton.disabled = true

func goToBranch(branch):
	if dialogTree.hasBranch(branch):
		process_branch(branch)
		update_tree_view()
	else:
		printerr("Branch " + branch + " does not exist!")

func enableOperationButtons():
	%SaveFileButton.disabled = false
	%SaveAsFileButton.disabled = false
	%DialogStringBoxContainer/NewDialogStringButton.disabled = false
	%DialogStringBoxContainer/DeleteDialogStringButton.disabled = false
	%DialogStringBoxContainer/DialogStringConditionsButton.disabled = false
	%DialogTextEdit.editable = true
	%AddOptionButton.disabled = false
	%BranchScriptButton.disabled = false

func getOptionContainer(index: int) -> VBoxContainer:
	return %DialogContentContainer.get_child(index + OPTIONS_OFFSET_IN_DIALOG_CONTENT_CONTAINER)

func updateDialogTextUI(newText):
	setDialogTextEdit(newText)
	updateStringConditionButton()
	
func setDialogTextEdit(newText):
	%DialogTextEdit.text = newText
	%NextStringButton.disabled = dialogTree.activeStringIndex + 1 >= dialogTree.getBranchStringsCount()
	%PrevStringButton.disabled = dialogTree.activeStringIndex <= 0

func updateStringConditionButton():
	var hasBranchConditions = dialogTree.hasBranchStringCondition()
	%DialogStringBoxContainer/DialogStringConditionsButton.setConditionsButtonActive(hasBranchConditions)

func newDialogString():
	dialogTree.addBranchEmptyString()
	dialogTree.jumpToActiveBranchString(dialogTree.getBranchStringsCount() - 1)
	addDialogIndicatorButton()
	updateDialogTextUI("")

func addDialogIndicatorButton(index: int = dialogTree.activeStringIndex):
	var newIndicatorButton = %DialogStringIndicatorButton.duplicate()
	newIndicatorButton.add_to_group("DialogEditorStringIndicatorButtons")
	newIndicatorButton.visible = true
	newIndicatorButton.pressedStringIndicatorButton.connect(_on_string_indicator_button_pressed)
	%DialogStringBoxContainer.add_child(newIndicatorButton)
	%DialogStringBoxContainer.move_child(newIndicatorButton, index)
	setDialogIndicatorButtonActive()

func setDialogIndicatorButtonActive():
	var buttonGroupArray = get_tree().get_nodes_in_group("DialogEditorStringIndicatorButtons")
	for button in buttonGroupArray:
		button.flat = true
	buttonGroupArray[dialogTree.activeStringIndex].flat = false

func deleteDialogString():
	if not dialogTree.hasBranchStrings():
		return
	var stringIndicatorButtonArray = get_tree().get_nodes_in_group("DialogEditorStringIndicatorButtons")		
	if dialogTree.getBranchStringsCount() == 1:
		dialogTree.deleteBranchStrings()
		%PrevStringButton.disabled = true
		%NextStringButton.disabled = true
		%DialogTextEdit.text = ""
		return
	
	dialogTree.deleteBranchString()
	%DialogStringBoxContainer.remove_child(stringIndicatorButtonArray[dialogTree.activeStringIndex])
	if dialogTree.activeStringIndex >= dialogTree.getBranchStringsCount():
		dialogTree.activeStringIndex = dialogTree.getBranchStringsCount() - 1
	updateDialogTextUI(dialogTree.getBranchStringText())
	setDialogIndicatorButtonActive()

func deleteOption(index):
	dialogTree.deleteOption(index)
	process_branch()

func moveOption(index, swapIndex):
	dialogTree.swapOptions(index, swapIndex)
	process_branch()

func createNewBranch(branchName):
	if branchName.is_empty():
		return
	dialogTree.addBranch(branchName)
	update_tree_view()
	activeBranchPickerButton.text = branchName
	activeBranchPickerButton.activateGoButton()

func openBranchPicker(BranchPickerButton: Button):
	if activeBranchPickerButton != null:
		return
	_branchPickerWindow.show()
	activeBranchPickerButton = BranchPickerButton
	_branchPickerItemList.grab_focus()

func openOptionsConditions(optionIndex):
	if activeOption > -1:
		getOptionContainer(activeOption).setOptionConditionsButtonActive(dialogTree.hasOptionConditions(activeOption))

	activeOption = optionIndex
	%OptionSettingsContainer/OptionSettingsVContainer/CodeEditLabel.text = "Condition Settings for Dialog Option"
	%OptionSettingsContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.text = dialogTree.getOptionConditions(activeOption)
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.grab_focus()
	getOptionContainer(activeOption).setOptionConditionsButtonSelected()

func _on_new_file_button_pressed():
	%NewFileDialog.show()

func _on_load_file_button_pressed():
	%OpenFileDialog.show()

func _on_save_file_button_pressed():
	save_dialog_file()

func _on_save_as_file_button_pressed():
	%SaveFileDialog.show()

func _on_new_file_dialog_file_selected(newDialogFilePath):
	create_new_dialog_file(newDialogFilePath)

func _on_file_dialog_file_selected(dialogJsonPath):
	load_dialog_file(dialogJsonPath)

func _on_save_file_dialog_file_selected(saveFilePath):
	writeDialogToFile(saveFilePath)

func _on_next_string_button_pressed():
	dialogTree.advanceActiveBranchString()
	updateDialogTextUI(dialogTree.getBranchStringText())
	%PrevStringButton.disabled = false
	setDialogIndicatorButtonActive()
	
func _on_prev_string_button_pressed():
	dialogTree.revertActiveBranchString()
	updateDialogTextUI(dialogTree.getBranchStringText())
	%NextStringButton.disabled = false
	setDialogIndicatorButtonActive()

func _on_string_indicator_button_pressed(index):
	dialogTree.jumpToActiveBranchString(index)
	updateDialogTextUI(dialogTree.getBranchStringText())
	setDialogIndicatorButtonActive()

func _on_new_dialog_string_button_pressed():
	newDialogString()
	
func _on_delete_dialog_string_button_pressed():
	deleteDialogString()

func _on_dialog_text_edit_text_changed():
	# NOTE: This is to not immeditatly copy text into DialogTree, for when we want to implement undo/redo
	# if timer != null:
	#	timer.stop()
	# timer = $Timer
	# timer.set_wait_time(1)
	# timer.set_one_shot(true)
	# timer.connect("timeout", self, apply_dialog_text_edit_changes)
	# timer.start
	dialogTree.setBranchStringText(%DialogTextEdit.text)

# called via signal
func apply_option_line_edit_changes(index, newText):
	dialogTree.setOptionsString(index, newText)
	
func _on_add_option_button_pressed():
	dialogTree.addOption()
	print("added new option")
	process_branch()

func _on_tree_item_activated():
	var treeItem = %Tree.get_selected()
	if treeItem.get_child_count() == 0: # dont select the tree parent
		var branch = treeItem.get_text(0)
		process_branch(branch)

func _on_save_conditions_or_script_calls_button_pressed():
	%OptionSettingsContainer.visible = false
	var codeText = %OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.text
	var codeNotEmpty = codeText != ""
	if activeOption > -1:
		dialogTree.setOptionConditions(activeOption, codeText)
		getOptionContainer(activeOption).setOptionConditionsButtonActive(codeNotEmpty)
	elif activeOption == -2: # Save branch string condition
		dialogTree.setBranchStringConditions(codeText)
		%DialogStringBoxContainer/DialogStringConditionsButton.setConditionsButtonActive(codeNotEmpty)
	else:
		dialogTree.setBranchScript(codeText)
		%BranchScriptButton.setScriptButtonActive(codeNotEmpty)
	activeOption = -1 # Reset to "nothing selected"

func _on_dialog_string_conditions_button_pressed():
	%OptionSettingsContainer/OptionSettingsVContainer/CodeEditLabel.text = "Condition Settings for Branch String"
	%OptionSettingsContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.text = dialogTree.getBranchStringCondition()
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.grab_focus()
	%DialogStringBoxContainer/DialogStringConditionsButton.setConditionsButtonSelected()
	activeOption = -2 # Magic number; This means "branch string conditions"

func _on_branch_script_button_pressed():
	%OptionSettingsContainer/OptionSettingsVContainer/CodeEditLabel.text = "Script for Dialog Branch"	
	%OptionSettingsContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer.visible = true
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.text = dialogTree.getBranchScript()
	%OptionSettingsContainer/OptionSettingsVContainer/OptionSettingsCodeEdit.grab_focus()
	%BranchScriptButton.setScriptButtonSelected()
