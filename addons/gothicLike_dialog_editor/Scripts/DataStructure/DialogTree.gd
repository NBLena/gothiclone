class_name DialogTree

# the actual tree data structure
var tree: Dictionary = {}
var filePath: String = ""

# constants
const CURRENT_VERSION = 3
const CLEAN_TREE: Dictionary = {"version": CURRENT_VERSION, "branches": {"start": {"strings": [{"text": "Hi!"}], "options": [{"string": "END", "next": null}]}}}
const CLEAN_OPTION: Dictionary = {"string": "", "next": null, "conditions": ""}
const CLEAN_BRANCH: Dictionary = {"strings": []}
const CLEAN_STRING: Dictionary = {"text": ""}

# state
var activeBranch = "start"
var activeStringIndex = 0

# State functions
func advanceActiveBranchString() -> void:
	activeStringIndex += 1

func revertActiveBranchString() -> void:
	activeStringIndex -= 1

func jumpToActiveBranchString(index: int) -> void:
	activeStringIndex = index

# Data fuctions
func setTree(newTreeData: Dictionary) -> void:
	tree = newTreeData

func addBranch(newBranch: String) -> void:
	tree.branches[newBranch] = CLEAN_BRANCH.duplicate()

func getBranch(branch: String = activeBranch) -> Dictionary:
	return tree.branches[branch]

func getBranches() -> Dictionary:
	return tree.branches

func getBranchName(branchIndex: int) -> String:
	return getBranches().keys()[branchIndex - 1]

func getBranchStrings(branch: String = activeBranch) -> Array:
	var treeBranch = getBranch(branch)
	if treeBranch.has("strings"):
		return treeBranch.get("strings")
	return []

func hasBranchStrings(branch: String = activeBranch) -> bool:
	return getBranch(branch).has("strings") and getBranchStringsCount(branch) > 0

func hasReachedEndOfBranchStrings(branch: String = activeBranch) -> bool:
	return activeStringIndex > getBranchStringsCount() - 1

func getBranchStringText(branch: String = activeBranch, index: int = activeStringIndex) -> String:
	return getBranchStrings(branch)[index].text

func getBranchString(branch: String = activeBranch, index: int = activeStringIndex) -> Dictionary:
	if len(getBranchStrings(branch)) > 0:
		return getBranchStrings(branch)[index]
	return CLEAN_STRING

func setBranchStringText(string: String, branch: String = activeBranch, index: int = activeStringIndex) -> void:
	if hasBranchStrings(branch):
		getBranchString(branch, index).text = string
	else:
		tree.branches[branch].strings = [{"text": string}]

func addBranchString(newBranch: Dictionary, branch: String = activeBranch) -> void:
	if hasBranchStrings(branch):
		getBranchStrings(branch).append(newBranch)
	else:
		getBranch(branch).strings = [newBranch]

func addBranchEmptyString(branch: String = activeBranch) -> void:
	if getBranchStrings(branch):
		getBranchStrings(branch).append(CLEAN_STRING.duplicate())
	else:
		getBranch(branch).strings = [CLEAN_STRING.duplicate()]
	
func hasBranchStringCondition(branch: String = activeBranch, index: int = activeStringIndex) -> bool:
	return getBranchStringsCount(branch) > 0 and \
		getBranch(branch).strings[index].has("conditions") and \
		getBranchStringCondition(branch, index) != ""

func getBranchStringCondition(branch: String = activeBranch, index: int = activeStringIndex) -> String:
	var string = getBranchString(activeBranch, index)
	if string.has("conditions") and string.conditions:
		return string.conditions
	return ""

func setBranchStringConditions(newConditions: String, branch: String = activeBranch, index: int = activeStringIndex) -> void:
	getBranchString(activeBranch, index).conditions = newConditions

func getBranchScript(branch: String = activeBranch) -> String:
	var script = getBranch(branch).get("script")
	if script:
		return script
	return ""

func setBranchScript(script: String, branch: String = activeBranch) -> void:
	getBranch(branch).script = script
	print("Set script!")
	print(getBranchScript(activeBranch))

func hasBranchScript(branch: String =  activeBranch) -> bool:
	return getBranch(branch).has("script") and getBranchScript(branch) != ""

func getOptions(branch: String = activeBranch) -> Array:
	var options = getBranch(branch).get("options")
	if options:
		return options
	return []

func hasOptions(branch: String = activeBranch) -> bool:
	return getBranch(branch).has("options") and getOptionsCount(branch) > 0

func getOption(optionIndex: int, branch: String = activeBranch) -> Dictionary:
	return getOptions(branch)[optionIndex]

func getOptionsString(optionIndex: int, branch: String = activeBranch) -> String:
	return getOption(optionIndex).string

func setOptionsString(optionIndex: int, string: String, branch: String = activeBranch) -> void:
	getOption(optionIndex).string = string

func addOption(branch: String = activeBranch) -> void:
	var newOptions = []
	if hasOptions(branch):
		newOptions = getOptions(branch)
	newOptions.append(CLEAN_OPTION.duplicate())
	getBranch(branch).options = newOptions

func deleteOption(optionIndex: int, branch: String = activeBranch) -> void:
	getOptions(branch).remove_at(optionIndex)

func swapOptions(index: int, swapIndex: int, branch: String = activeBranch) -> void:
	var holdOption = getOption(index, branch)
	getBranch(branch).options[index] = getBranch(branch).options[swapIndex]
	getBranch(branch).options[swapIndex] = holdOption

func getBranchNext(branch: String = activeBranch) -> String:
	var nextString = getBranch(branch).get("next")
	return nextString if nextString else ""

func setBranchNext(nextTarget: String, branch: String = activeBranch) -> void:
	getBranch(branch).next = nextTarget

func hasBranchNext(branch: String = activeBranch) -> bool:
	return getBranch(branch).has("next") and getBranch(branch).next

func hasBranch(branch: String = activeBranch) -> bool:
	return tree.branches.has(branch)

func hasOptionNext(optionIndex: int, branch: String = activeBranch) -> bool:
	return getOption(optionIndex, branch).has("next") and getOption(optionIndex, branch).next

func getOptionNext(optionIndex: int, branch: String = activeBranch) -> String:
	var nextString = getOption(optionIndex, branch).get("next")
	return nextString if nextString else ""

func setOptionNext(optionIndex: int, nextTarget: String, branch: String = activeBranch) -> void:
	getOption(optionIndex, branch).next = nextTarget


func getOptionsCount(branch: String = activeBranch) -> int:
	return len(getBranch(branch).options)

func getOptionConditions(optionIndex: int, branch: String = activeBranch) -> String:
	var option = getOption(optionIndex, branch)
	if option.has("conditions") and option.conditions:
		return option.conditions
	return ""

func setOptionConditions(optionIndex: int, newConditions: String, branch: String = activeBranch) -> void:
	getOption(optionIndex, branch).conditions = newConditions

func hasOptionConditions(optionIndex: int, branch: String = activeBranch) -> bool:
	if getOption(optionIndex, branch).has("conditions"):
		var conditions = getOptionConditions(optionIndex, branch)
		return conditions != null and conditions != ""
	return false

func getBranchStringsCount(branch: String = activeBranch) -> int:
	var counted = len(getBranchStrings(branch))
	if counted == 1 and getBranchString(activeBranch, 0).text == "":
		# also return none if the only branch string has an empty text string
		return 0
	return counted

func deleteBranchStrings(branch: String = activeBranch) -> void:
	getBranch(branch).strings = null

func deleteBranchString(index: int = activeStringIndex, branch: String = activeBranch) -> void:
	getBranchStrings(branch).remove_at(index)

func isTreeOutdated() -> bool:
	return getVersion() < CURRENT_VERSION

func getVersion() -> int:
	return tree["version"]
	
func migrateDialogTree():
	print("Tree is outdated! Starting migration...")
	
	if tree.version == 1:
		print("Migrating from version 1.... copying tree into branches dictionary")
		var treeCopy = tree.duplicate()
		tree.clear()
		tree.branches = treeCopy as Dictionary
		tree.branches.erase("version")
	
	if tree.version == 2:
		print("Migrating from version 2... turning branch strings into dictionaries")
		for branchName in getBranches().keys():
			var stringsCopy: Array = getBranchStrings(branchName)
			print("Migrating ", branchName)
			getBranch(branchName).strings = null
			for string in stringsCopy:
				var stringDict = {"text": string}
				addBranchString(stringDict, branchName)
	
	tree.version = CURRENT_VERSION
	print("New Tree: ", tree)
