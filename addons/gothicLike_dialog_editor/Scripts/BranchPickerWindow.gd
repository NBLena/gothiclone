@tool
extends Window

@onready var _branchPickerItemList = %BranchPickerItemList
@onready var editor = get_parent()

func closeBranchPickerWindow():
	hide()
	editor.activeBranchPickerButton = null

func setNextViaBranchPicker(nextBranch):
	if editor.activeBranchPickerButton.name.begins_with("Option"):
		var optionIndex = editor.activeBranchPickerButton.get_node("..").my_index()
		editor.dialogTree.setOptionNext(optionIndex, nextBranch)
	else:
		editor.dialogTree.setBranchNext(nextBranch)

func createNewBranch(newBranch):
	editor.createNewBranch(newBranch)
	setNextViaBranchPicker(newBranch)
	closeBranchPickerWindow()

func _on_close_requested():
	closeBranchPickerWindow()

func _on_filter_line_edit_text_changed(filter_term):
	_branchPickerItemList.clear()
	for branch in editor.dialogTree.tree:
		if branch.contains(filter_term) or filter_term.is_empty():
			_branchPickerItemList.add_item(branch)

func _on_segment_picker_item_list_item_activated(index):
	if index == 0:
		editor.activeBranchPickerButton.text = "[Empty]"
		editor.activeBranchPickerButton.deactivateGoButton()
		setNextViaBranchPicker(null)
	else:
		editor.activeBranchPickerButton.text = editor.dialogTree.getBranchName(index)
		editor.activeBranchPickerButton.activateGoButton()
		setNextViaBranchPicker(editor.dialogTree.getBranchName(index))
	closeBranchPickerWindow()

func _on_new_segment_line_edit_text_changed(new_text):
	%NewSegmentButton.disabled = new_text.is_empty()

func _on_new_segment_line_edit_text_submitted(new_branch_name):
	createNewBranch(new_branch_name)

func _on_new_segment_button_pressed():
	var new_segment_name = %NewSegmentLineEdit.text
	createNewBranch(new_segment_name)
