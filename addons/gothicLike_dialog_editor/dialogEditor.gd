@tool
extends EditorPlugin

var dock = null

func _enter_tree():
	# Initialization of the plugin goes here.
	dock = preload("res://addons/gothicLike_dialog_editor/Scenes/DialogEditorDock.tscn").instantiate()
	add_control_to_bottom_panel(dock, "Dialog Editor")
	
	# Uncomment following line for debugging the dock, so you dont have to open it every time it is re-loaded
	make_bottom_panel_item_visible(dock)

func _exit_tree():
	# Clean-up of the plugin goes here.
	remove_control_from_bottom_panel(dock)
	dock.queue_free()

func _build() -> bool:
	if dock != null:
		dock.save_dialog_file()
	return true # if return false, project does not run!
